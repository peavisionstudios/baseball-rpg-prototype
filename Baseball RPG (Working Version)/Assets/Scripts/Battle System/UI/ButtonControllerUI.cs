﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonControllerUI : MonoBehaviour, IPointerEnterHandler
{
    //Buttons to manipulate
    [SerializeField] Button swingAwayButton, takePitchButton, itemsButton, tricksButton;

    //Classes to use
    [SerializeField] HitChance hitChance;
    [SerializeField] TakeChance takeChance;
    [SerializeField] HitChanceSliderUI hitChanceSliderUI;
    private CritArrow critArrow;

    //Button press trackers
    private int timesClickedHit, timesClickedTake, timesClickedTrick;

    //Crit Result passed while destroying temp crit arrow
    public float critResult;

    // Start is called before the first frame update
    void Awake()
    {
        swingAwayButton.onClick.AddListener(RunHitChanceLoop);
        takePitchButton.onClick.AddListener(RunTakeChanceLoop);
        tricksButton.onClick.AddListener(RunTrickChanceLoop);
    }

    void Start()
    {
        timesClickedHit = 0;
        timesClickedTake = 0;
    }

    private void RunHitChanceLoop()
    {
        timesClickedTake = 0;
        timesClickedTrick = 0;
        hitChance.IsTrickFalse();

        timesClickedHit++;

        if (timesClickedHit == 1)
        {
            hitChance.ShowHitScale();
        }
        else if (timesClickedHit == 2)
        {
            hitChanceSliderUI.EstablishCritScale();
            hitChanceSliderUI.EstablishCritArrow();
        }
        else if (timesClickedHit >= 3)
        {
            timesClickedHit = 0;
            hitChance.RunFullHitLoop();
            CheckForCritArrow();
            hitChanceSliderUI.ResetScale();

        }
    }
    private void RunTakeChanceLoop()
    {
        timesClickedHit = 0;
        timesClickedTrick = 0;
        hitChance.IsTrickFalse();

        timesClickedTake++;

        if(timesClickedTake == 1)
        {
            takeChance.ShowTakeScale();
        }
        else if(timesClickedTake >= 2)
        {
            timesClickedTake = 0;
            takeChance.RunFullTakeLoop();
        }

    }
    private void RunTrickChanceLoop()
    {
        timesClickedTake = 0;
        timesClickedHit = 0;

        timesClickedTrick++;

        if (timesClickedTrick == 1)
        {
            hitChance.IsTrickTrue();
            hitChance.ShowHitScale();
        }
        else if (timesClickedTrick == 2)
        {
            hitChanceSliderUI.EstablishCritScale();
            hitChanceSliderUI.EstablishCritArrow();
        }
        else if (timesClickedTrick >= 3)
        {
            timesClickedTrick = 0;
            hitChance.RunFullHitLoop();
            CheckForCritArrow();
            hitChanceSliderUI.ResetScale();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        takeChance.ShowTakeScale();
    }

    private void CheckForCritArrow()
    {
        if (GameObject.Find("CritArrow(Clone)") != null)
        {
            critArrow = GameObject.Find("CritArrow(Clone)").GetComponent<CritArrow>();
            critResult = critArrow.CritResultAndDestroy();
        }
    }

}
