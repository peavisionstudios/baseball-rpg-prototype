﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultTextInstanceUI : MonoBehaviour
{
    private Rigidbody2D rigidBody;
    private GameObject currentGameObject;
    private void Start()
    {
        currentGameObject = GetComponent<GameObject>();
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(0, 600);
        Destroy(gameObject, 1.5f);
    }

}
