﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResultTextUI : MonoBehaviour
{
    [SerializeField] Text resultText;
    [SerializeField] Canvas canvas;

    private string currentResultText;
    private float canvasWidth, canvasHeight;

    // Start is called before the first frame update
    void Start()
    {
        canvasWidth = canvas.GetComponent<RectTransform>().rect.width;
        canvasHeight = canvas.GetComponent<RectTransform>().rect.height;

    }
    public void ShowResultText(string atbatResult)
    {
        Text resultTextTemp = Instantiate(resultText, new Vector3(canvasWidth / 2, canvasHeight / 2, transform.position.z), Quaternion.identity);
        resultTextTemp.transform.SetParent(canvas.transform);
        resultTextTemp.text = atbatResult;

    }

}
