﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BoxScoreUI : MonoBehaviour
{
    //Derived classes

    [SerializeField] BoxScore boxScore;
    [SerializeField] PlayerCharacterDatabase playerCharacterDatabase;

    //Text components to manipulate

    private TextMeshProUGUI 
        homeScoreUI, awayScoreUI,  //Current score
        strikesUI, ballsUI, outsUI, //Pitch count
        currentInningUI, //Current inning number
        currentBatterUI; //Current batter variable (Batter Name + Batter Team)

    //Image components to manipulate

    private Image 
        firstBaseUI, secondBaseUI, thirdBaseUI, //On-Base diamond
        topBottomInningArrowUI; //Arrow that indicates top or bottom of inning

    //GameObjects to manipulate

    [SerializeField] GameObject 
        homeScoreObject, awayScoreObject, 
        strikesObject, ballsObject, outsObject, 
        currentInningObject, 
        currentBatterObject, 
        firstBaseUIObject, secondBaseUIObject, thirdBaseUIObject, 
        topBottomInningArrowUIObject;



    //Image state variables

    private bool firstBaseOccupied, secondBaseOccupied, thirdBaseOccupied;

    //Sprites to use

    [SerializeField] Sprite 
        baseOccupiedTrue, baseOccupiedFalse,
        topInningArrow, bottomInningArrow;

    // Start is called before the first frame update
    void Start()
    {
        GetAllComponents();

        UpdateBoxScoreUI();

    }

    // Update is called once per frame
    void Update()
    {
        UpdateBoxScoreUI();
    }
    private void GetAllComponents()
    {
        homeScoreUI = homeScoreObject.GetComponent<TextMeshProUGUI>();
        awayScoreUI = awayScoreObject.GetComponent<TextMeshProUGUI>();
        strikesUI = strikesObject.GetComponent<TextMeshProUGUI>();
        ballsUI = ballsObject.GetComponent<TextMeshProUGUI>();
        outsUI = outsObject.GetComponent<TextMeshProUGUI>();
        currentInningUI = currentInningObject.GetComponent<TextMeshProUGUI>();
        currentBatterUI = currentBatterObject.GetComponent<TextMeshProUGUI>();

        firstBaseUI = firstBaseUIObject.GetComponent<Image>();
        secondBaseUI = secondBaseUIObject.GetComponent<Image>();
        thirdBaseUI = thirdBaseUIObject.GetComponent<Image>();
        topBottomInningArrowUI = topBottomInningArrowUIObject.GetComponent<Image>();
    }
    public void UpdateBoxScoreUI()
    {
        homeScoreUI.text = boxScore.homeScore.ToString();
        awayScoreUI.text = boxScore.awayScore.ToString();
        strikesUI.text = boxScore.strikes.ToString();
        ballsUI.text = boxScore.balls.ToString();
        outsUI.text = (boxScore.outs.ToString() + " Out");
        currentInningUI.text = boxScore.currentInning.ToString();
        currentBatterUI.text = (boxScore.currentHitter + " - " + boxScore.currentTeam);

        firstBaseOccupied = playerCharacterDatabase.GetBaseOccupiedBool(1);
        secondBaseOccupied = playerCharacterDatabase.GetBaseOccupiedBool(2);
        thirdBaseOccupied = playerCharacterDatabase.GetBaseOccupiedBool(3);

        UpdateOnBaseDiamond();
        UpdateTopBottomInningArrow();

    }
    private void UpdateOnBaseDiamond()
    {
        if (firstBaseOccupied == true)
        {
            firstBaseUI.sprite = baseOccupiedTrue;
        }
        else
        {
            firstBaseUI.sprite = baseOccupiedFalse;
        }

        if (secondBaseOccupied == true)
        {
            secondBaseUI.sprite = baseOccupiedTrue;
        }
        else
        {
            secondBaseUI.sprite = baseOccupiedFalse;
        }

        if (thirdBaseOccupied == true)
        {
            thirdBaseUI.sprite = baseOccupiedTrue;
        }
        else
        {
            thirdBaseUI.sprite = baseOccupiedFalse;
        }
    }
    private void UpdateTopBottomInningArrow()
    {
        if(boxScore.homeTeamUp == true)
        {
            topBottomInningArrowUI.sprite = bottomInningArrow;
        }
        
        if(boxScore.awayTeamUp == true)
        {
            topBottomInningArrowUI.sprite = topInningArrow;
        }
    }
}
