﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CritArrow : MonoBehaviour
{
    private Rigidbody critArrowRigidBody;
    private float startingLocationX, finalLocationX, currentX, barWidth, critBarResult, halfCritArrowWidth;

    // Start is called before the first frame update
    void Start()
    {
        critArrowRigidBody = GetComponent<Rigidbody>();
        barWidth = GameObject.Find("SelectionUI").GetComponent<RectTransform>().rect.width;

        startingLocationX = GetComponent<RectTransform>().position.x;
        finalLocationX = startingLocationX + barWidth;

        Debug.Log(barWidth);
        Debug.Log(startingLocationX);
        Debug.Log(finalLocationX);

        StartCoroutine(LaunchCritArrow());
    }

    // Update is called once per frame
    void Update()
    {
        currentX = GetComponent<RectTransform>().position.x;
        halfCritArrowWidth = GetComponent<RectTransform>().rect.width / 2;

        if (currentX - halfCritArrowWidth >= finalLocationX)
        {
            CritResultAndDestroy();
        }
    }

    public float CritResultAndDestroy()
    {
        critBarResult = GetComponent<RectTransform>().position.x;
        Destroy(gameObject);
        Debug.Log(critBarResult);
        return critBarResult;

    }

    IEnumerator LaunchCritArrow()
    {
        yield return new WaitForSeconds(1);
        critArrowRigidBody.velocity = new Vector3(2000, 0, 0);

    }
}
