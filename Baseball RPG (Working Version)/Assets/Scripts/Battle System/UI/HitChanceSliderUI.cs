﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitChanceSliderUI : MonoBehaviour
{
    //Class References
    [SerializeField] Canvas canvas;
    [SerializeField] HitChance hitChance;
    [SerializeField] TakeChance takeChance;

    //Exposed Config settings

    [SerializeField] int sliderSpeedModfier = 5;

    public float critBarResult;

    private float
        screenWidth, screenHeight,
        barWidth, barHeight,
        centerToTop, centerToSide,
        critScaleCenter,
        singleScaleSize, doubleScaleSize, tripleScaleSize, homerunScaleSize, 
        ballScaleSize, strikeScaleSize, outScaleSize, 
        missScaleSize, normalScaleSize, critScaleSize;

    [SerializeField] GameObject selectionUI;
    [SerializeField] Image 
        fullBarImage, critArrowImage, critArrowTemp,
        singleScaleImage, doubleScaleImage, tripleScaleImage, homerunScaleImage, 
        ballScaleImage, strikeScaleImage, outScaleImage, 
        missScaleImage, normalScaleImage, critScaleImage;

    private RectTransform 
        singleScaleTransform, doubleScaleTransform, tripleScaleTransform, homerunScaleTransform, 
        ballScaleTransform, strikeScaleTransform, outScaleTransform, 
        missScaleTransform, normalScaleTransform, critScaleTransform;

    private Rigidbody2D critArrowRigidBody;

    private Vector2 
        fullBarDefaultLocation, fullBarLeftSide, currentOffsetMaxY, 
        nextSingleOffsetMax, nextDoubleOffsetMax, nextTripleOffsetMax, nextHomerunOffsetMax, 
        nextBallOffsetMax, nextStrikeOffsetMax, nextOutOffsetMax,
        nextNormalLocation, nextCritLocation, nextMissOffsetMax;

    // Start is called before the first frame update
    void Start()
    {
        screenWidth = canvas.GetComponent<RectTransform>().rect.width;
        screenHeight = canvas.GetComponent<RectTransform>().rect.height;

        barWidth = fullBarImage.GetComponent<RectTransform>().rect.width;
        barHeight = fullBarImage.GetComponent<RectTransform>().rect.height;

        Vector3[] fullBarCorners = new Vector3[4];
        fullBarImage.GetComponent<RectTransform>().GetLocalCorners(fullBarCorners);

        fullBarLeftSide = fullBarCorners[1];

        centerToTop = screenHeight / 2;
        centerToSide = screenWidth / 2;

        critScaleCenter = barWidth / 4;

        fullBarDefaultLocation = fullBarImage.GetComponent<RectTransform>().position;

        singleScaleTransform = singleScaleImage.GetComponent<RectTransform>();
        doubleScaleTransform = doubleScaleImage.GetComponent<RectTransform>();
        tripleScaleTransform = tripleScaleImage.GetComponent<RectTransform>();
        homerunScaleTransform = homerunScaleImage.GetComponent<RectTransform>();
        ballScaleTransform = ballScaleImage.GetComponent<RectTransform>();
        strikeScaleTransform = strikeScaleImage.GetComponent<RectTransform>();
        outScaleTransform = outScaleImage.GetComponent<RectTransform>();
        missScaleTransform = missScaleImage.GetComponent<RectTransform>();
        normalScaleTransform = normalScaleImage.GetComponent<RectTransform>();
        critScaleTransform = critScaleImage.GetComponent<RectTransform>();

        currentOffsetMaxY = singleScaleTransform.offsetMax;

        ResetScale();

    }

    // Update is called once per frame
    void Update()
    {
        //Hit Bar or Take Bar
        CheckSingleUpdates();
        CheckDoubleUpdates();
        CheckTripleUpdates();
        CheckHomerunUpdates();
        CheckBallUpdates();
        CheckStrikeUpdates();
        CheckOutUpdates();

        //Crit Bar
        CheckMissUpdates();
        CheckNormalUpdates();
        CheckCritUpdates();
    }

    public void EstablishHitScale()
    {
        //Determine pixel size for each slider section

        singleScaleSize = Mathf.Floor(barWidth * hitChance.finalSinglePercentage);
        doubleScaleSize = Mathf.Floor(barWidth * hitChance.finalDoublePercentage);
        tripleScaleSize = Mathf.Floor(barWidth * hitChance.finalTriplePercentage);
        homerunScaleSize = Mathf.Floor(barWidth * hitChance.finalHomerunPercentage);
        ballScaleSize = Mathf.Floor(barWidth * hitChance.finalBallPercentage);
        strikeScaleSize = Mathf.Floor(barWidth * hitChance.finalStrikePercentage);
        outScaleSize = Mathf.Floor(barWidth * hitChance.finalOutPercentage);

        Debug.Log(singleScaleSize + " " + doubleScaleSize + " " + tripleScaleSize + " " + homerunScaleSize + " " + ballScaleSize + " " + strikeScaleSize + " " + outScaleSize);


        //Establish scale

        nextSingleOffsetMax = new Vector2(singleScaleSize, currentOffsetMaxY.y);
        nextDoubleOffsetMax = new Vector2(nextSingleOffsetMax.x + doubleScaleSize, currentOffsetMaxY.y);
        nextTripleOffsetMax = new Vector2(nextDoubleOffsetMax.x + tripleScaleSize, currentOffsetMaxY.y);
        nextHomerunOffsetMax = new Vector2(nextTripleOffsetMax.x + homerunScaleSize, currentOffsetMaxY.y);
        nextBallOffsetMax = new Vector2(nextHomerunOffsetMax.x + ballScaleSize, currentOffsetMaxY.y);
        nextStrikeOffsetMax = new Vector2(nextBallOffsetMax.x + strikeScaleSize, currentOffsetMaxY.y);
        nextOutOffsetMax = new Vector2(barWidth, currentOffsetMaxY.y);
    }
    public void EstablishTakeScale()
    {
        //Determine pixel size for each slider section

        singleScaleSize = 0;
        doubleScaleSize = 0;
        tripleScaleSize = 0;
        homerunScaleSize = 0;
        ballScaleSize = Mathf.Floor(barWidth * takeChance.finalBallPercentage);
        strikeScaleSize = Mathf.Floor(barWidth * takeChance.finalStrikePercentage);
        outScaleSize = 0;

        //Establish scale

        nextSingleOffsetMax = new Vector2(singleScaleSize, currentOffsetMaxY.y);
        nextDoubleOffsetMax = new Vector2(nextSingleOffsetMax.x + doubleScaleSize, currentOffsetMaxY.y);
        nextTripleOffsetMax = new Vector2(nextDoubleOffsetMax.x + tripleScaleSize, currentOffsetMaxY.y);
        nextHomerunOffsetMax = new Vector2(nextTripleOffsetMax.x + homerunScaleSize, currentOffsetMaxY.y);
        nextBallOffsetMax = new Vector2(nextHomerunOffsetMax.x + ballScaleSize, currentOffsetMaxY.y);
        nextStrikeOffsetMax = new Vector2(nextBallOffsetMax.x + strikeScaleSize, currentOffsetMaxY.y);
        nextOutOffsetMax = new Vector2(nextStrikeOffsetMax.x + outScaleSize, currentOffsetMaxY.y);
    }
    public void EstablishCritScale()
    {
        //Determine pixel size for each slider section

        singleScaleSize = 0;
        doubleScaleSize = 0;
        tripleScaleSize = 0;
        homerunScaleSize = 0;
        ballScaleSize = 0;
        strikeScaleSize = 0;
        outScaleSize = 0;

        missScaleSize = barWidth;
        normalScaleSize = 300;
        critScaleSize = 50;


        //Establish scale

        nextSingleOffsetMax = new Vector2(singleScaleSize, currentOffsetMaxY.y);
        nextDoubleOffsetMax = new Vector2(nextSingleOffsetMax.x + doubleScaleSize, currentOffsetMaxY.y);
        nextTripleOffsetMax = new Vector2(nextDoubleOffsetMax.x + tripleScaleSize, currentOffsetMaxY.y);
        nextHomerunOffsetMax = new Vector2(nextTripleOffsetMax.x + homerunScaleSize, currentOffsetMaxY.y);
        nextBallOffsetMax = new Vector2(nextHomerunOffsetMax.x + ballScaleSize, currentOffsetMaxY.y);
        nextStrikeOffsetMax = new Vector2(nextBallOffsetMax.x + strikeScaleSize, currentOffsetMaxY.y);
        nextOutOffsetMax = new Vector2(nextStrikeOffsetMax.x + outScaleSize, currentOffsetMaxY.y);

        //Setup Crit Bar

        nextMissOffsetMax = new Vector2(missScaleSize, currentOffsetMaxY.y);

        critScaleTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, critScaleSize);

        //Set next locations

        nextNormalLocation = new Vector2(critScaleCenter, currentOffsetMaxY.y);
        nextCritLocation = new Vector2(critScaleCenter, currentOffsetMaxY.y);


    }
    public void EstablishCritArrow()
    {
        critArrowTemp = Instantiate(critArrowImage, new Vector3((fullBarDefaultLocation.x - (barWidth / 2)), fullBarDefaultLocation.y + 80, transform.position.z), Quaternion.identity);
        critArrowTemp.transform.SetParent(selectionUI.transform);
    }

    public void ResetScale()
    {
        //Determine pixel size for each slider section

        singleScaleSize = 0;
        doubleScaleSize = 0;
        tripleScaleSize = 0;
        homerunScaleSize = 0;
        ballScaleSize = 0;
        strikeScaleSize = 0;
        outScaleSize = 0;
        missScaleSize = 0;
        normalScaleSize = 0;
        critScaleSize = 0;

        //Establish scale

        nextSingleOffsetMax = new Vector2(singleScaleSize, currentOffsetMaxY.y);
        nextDoubleOffsetMax = new Vector2(nextSingleOffsetMax.x + doubleScaleSize, currentOffsetMaxY.y);
        nextTripleOffsetMax = new Vector2(nextDoubleOffsetMax.x + tripleScaleSize, currentOffsetMaxY.y);
        nextHomerunOffsetMax = new Vector2(nextTripleOffsetMax.x + homerunScaleSize, currentOffsetMaxY.y);
        nextBallOffsetMax = new Vector2(nextHomerunOffsetMax.x + ballScaleSize, currentOffsetMaxY.y);
        nextStrikeOffsetMax = new Vector2(nextBallOffsetMax.x + strikeScaleSize, currentOffsetMaxY.y);
        nextOutOffsetMax = new Vector2(nextStrikeOffsetMax.x + outScaleSize, currentOffsetMaxY.y);
        nextMissOffsetMax = new Vector2(missScaleSize, currentOffsetMaxY.y);

        nextNormalLocation = fullBarLeftSide;
        nextCritLocation = fullBarLeftSide;

    }

    //Hits
    private void CheckSingleUpdates()
    {
        //Check single updates
        if (singleScaleTransform.offsetMax.x - sliderSpeedModfier > nextSingleOffsetMax.x)
        {
            singleScaleTransform.offsetMax = new Vector2(singleScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (singleScaleTransform.offsetMax.x >= nextSingleOffsetMax.x)
        {
            singleScaleTransform.offsetMax = nextSingleOffsetMax;
        }


        if (singleScaleTransform.offsetMax.x < nextSingleOffsetMax.x)
        {
            singleScaleTransform.offsetMax = new Vector2(singleScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (singleScaleTransform.offsetMax.x <= nextSingleOffsetMax.x)
        {
            singleScaleTransform.offsetMax = nextSingleOffsetMax;
        }
    }
    private void CheckDoubleUpdates()
    {
        //Check double updates
        if (doubleScaleTransform.offsetMax.x - sliderSpeedModfier > nextDoubleOffsetMax.x)
        {
            doubleScaleTransform.offsetMax = new Vector2(doubleScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (doubleScaleTransform.offsetMax.x >= nextDoubleOffsetMax.x)
        {
            doubleScaleTransform.offsetMax = nextDoubleOffsetMax;
        }


        if (doubleScaleTransform.offsetMax.x < nextDoubleOffsetMax.x)
        {
            doubleScaleTransform.offsetMax = new Vector2(doubleScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (doubleScaleTransform.offsetMax.x <= nextDoubleOffsetMax.x)
        {
            doubleScaleTransform.offsetMax = nextDoubleOffsetMax;
        }
    }
    private void CheckTripleUpdates()
    {
        //Check triple updates
        if (tripleScaleTransform.offsetMax.x - sliderSpeedModfier > nextTripleOffsetMax.x)
        {
            tripleScaleTransform.offsetMax = new Vector2(tripleScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (tripleScaleTransform.offsetMax.x >= nextTripleOffsetMax.x)
        {
            tripleScaleTransform.offsetMax = nextTripleOffsetMax;
        }


        if (tripleScaleTransform.offsetMax.x < nextTripleOffsetMax.x)
        {
            tripleScaleTransform.offsetMax = new Vector2(tripleScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (tripleScaleTransform.offsetMax.x <= nextTripleOffsetMax.x)
        {
            tripleScaleTransform.offsetMax = nextTripleOffsetMax;
        }
    }
    private void CheckHomerunUpdates()
    {
        //Check homerun updates
        if (homerunScaleTransform.offsetMax.x - sliderSpeedModfier > nextHomerunOffsetMax.x)
        {
            homerunScaleTransform.offsetMax = new Vector2(homerunScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (homerunScaleTransform.offsetMax.x >= nextHomerunOffsetMax.x)
        {
            homerunScaleTransform.offsetMax = nextHomerunOffsetMax;
        }


        if (homerunScaleTransform.offsetMax.x < nextHomerunOffsetMax.x)
        {
            homerunScaleTransform.offsetMax = new Vector2(homerunScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (homerunScaleTransform.offsetMax.x <= nextHomerunOffsetMax.x)
        {
            homerunScaleTransform.offsetMax = nextHomerunOffsetMax;
        }
    }

    //Crit Scale
    private void CheckCritUpdates()
    {
        //Check Crit Updates
        if (critScaleTransform.localPosition.x + sliderSpeedModfier < nextCritLocation.x)
        {
            critScaleTransform.localPosition = new Vector2(critScaleTransform.localPosition.x + sliderSpeedModfier, critScaleTransform.localPosition.y);
        }
        else if (critScaleTransform.localPosition.x <= nextCritLocation.x)
        {
            critScaleTransform.localPosition = new Vector2(critScaleCenter, critScaleTransform.localPosition.y);
        }

        if (critScaleTransform.localPosition.x - sliderSpeedModfier > nextCritLocation.x)
        {
            critScaleTransform.localPosition = new Vector2(critScaleTransform.localPosition.x - sliderSpeedModfier, critScaleTransform.localPosition.y);
        }

        if (critScaleTransform.sizeDelta.x < critScaleSize)
        {
            critScaleTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, critScaleTransform.sizeDelta.x + (sliderSpeedModfier/10));
        }
        else if (critScaleTransform.sizeDelta.x > critScaleSize)
        {
            critScaleTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, critScaleTransform.sizeDelta.x - (sliderSpeedModfier/10));
        }

    }
    private void CheckNormalUpdates()
    {
        //Check Normal Updates
        if (normalScaleTransform.localPosition.x + sliderSpeedModfier < nextNormalLocation.x)
        {
            normalScaleTransform.localPosition = new Vector2(normalScaleTransform.localPosition.x + sliderSpeedModfier, normalScaleTransform.localPosition.y);
        }
        else if (normalScaleTransform.localPosition.x <= nextNormalLocation.x)
        {
            normalScaleTransform.localPosition = new Vector2(critScaleCenter, normalScaleTransform.localPosition.y);
        }

        if (normalScaleTransform.localPosition.x - sliderSpeedModfier > nextNormalLocation.x)
        {
            normalScaleTransform.localPosition = new Vector2(normalScaleTransform.localPosition.x - sliderSpeedModfier, normalScaleTransform.localPosition.y);
        }

        if (normalScaleTransform.sizeDelta.x < normalScaleSize)
        {
            normalScaleTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, normalScaleTransform.sizeDelta.x + sliderSpeedModfier);
        }
        else if (normalScaleTransform.sizeDelta.x > normalScaleSize)
        {
            normalScaleTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, normalScaleTransform.sizeDelta.x - sliderSpeedModfier);
        }
    }
    private void CheckMissUpdates()
    {
        //Check Miss Updates
        if (missScaleTransform.offsetMax.x - sliderSpeedModfier > nextMissOffsetMax.x)
        {
            missScaleTransform.offsetMax = new Vector2(missScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (missScaleTransform.offsetMax.x >= nextMissOffsetMax.x)
        {
            missScaleTransform.offsetMax = nextMissOffsetMax;
        }


        if (missScaleTransform.offsetMax.x < nextMissOffsetMax.x)
        {
            missScaleTransform.offsetMax = new Vector2(missScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (missScaleTransform.offsetMax.x <= nextMissOffsetMax.x)
        {
            missScaleTransform.offsetMax = nextMissOffsetMax;
        }
    }

    //Other
    private void CheckOutUpdates()
    {
        //Check Out updates
        if (outScaleTransform.offsetMax.x - sliderSpeedModfier > nextOutOffsetMax.x)
        {
            outScaleTransform.offsetMax = new Vector2(outScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (outScaleTransform.offsetMax.x >= nextOutOffsetMax.x)
        {
            outScaleTransform.offsetMax = nextOutOffsetMax;
        }


        if (outScaleTransform.offsetMax.x < nextOutOffsetMax.x)
        {
            outScaleTransform.offsetMax = new Vector2(outScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (outScaleTransform.offsetMax.x <= nextOutOffsetMax.x)
        {
            outScaleTransform.offsetMax = nextOutOffsetMax;
        }
    }
    private void CheckStrikeUpdates()
    {
        //Check strike updates
        if (strikeScaleTransform.offsetMax.x - sliderSpeedModfier > nextStrikeOffsetMax.x)
        {
            strikeScaleTransform.offsetMax = new Vector2(strikeScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (strikeScaleTransform.offsetMax.x >= nextStrikeOffsetMax.x)
        {
            strikeScaleTransform.offsetMax = nextStrikeOffsetMax;
        }


        if (strikeScaleTransform.offsetMax.x < nextStrikeOffsetMax.x)
        {
            strikeScaleTransform.offsetMax = new Vector2(strikeScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (strikeScaleTransform.offsetMax.x <= nextStrikeOffsetMax.x)
        {
            strikeScaleTransform.offsetMax = nextStrikeOffsetMax;
        }
    }
    private void CheckBallUpdates()
    {
        //Check ball updates
        if (ballScaleTransform.offsetMax.x - sliderSpeedModfier > nextBallOffsetMax.x)
        {
            ballScaleTransform.offsetMax = new Vector2(ballScaleTransform.offsetMax.x - sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (ballScaleTransform.offsetMax.x >= nextBallOffsetMax.x)
        {
            ballScaleTransform.offsetMax = nextBallOffsetMax;
        }


        if (ballScaleTransform.offsetMax.x < nextBallOffsetMax.x)
        {
            ballScaleTransform.offsetMax = new Vector2(ballScaleTransform.offsetMax.x + sliderSpeedModfier, currentOffsetMaxY.y);
        }
        else if (ballScaleTransform.offsetMax.x <= nextBallOffsetMax.x)
        {
            ballScaleTransform.offsetMax = nextBallOffsetMax;
        }
    }




}
