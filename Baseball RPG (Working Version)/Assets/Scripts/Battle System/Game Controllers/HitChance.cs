﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitChance : MonoBehaviour
{
    //Classes to use
    [SerializeField] BoxScore boxScore;
    [SerializeField] PlayerCharacterDatabase playerCharacterDatabase;
    [SerializeField] TricksDatabase tricksDatabase;
    [SerializeField] HitChanceSliderUI hitChanceSliderUI;
    [SerializeField] ResultTextUI resultTextUI;

    //Hit Algorithm Variables

    private bool isTrick;

    private int
        hitSingle, hitDouble, hitTriple, hitHomerun,
        playerLevel, playerBaseStat, playerContactModifier, playerSpeedModifier, playerPowerModifier, playerBaserunningModifier,
        levelDifferential, hitPercentage,

        //Trick Modifiers
        trickLevelModifier, trickPowerModifier, trickContactModifier, trickHitChanceModifier, trickSpeedModifier;
        private Tricks currentTrick;

    private float
        rawSinglePercent, singleScaleSize, singleHitScaleMin, singleHitScaleMax,
        rawDoublePercent, doubleScaleSize, doubleHitScaleMin, doubleHitScaleMax,
        rawTriplePercent, tripleScaleSize, tripleHitScaleMin, tripleHitScaleMax,
        rawHomerunPercent, homerunScaleSize, homerunHitScaleMin, homerunHitScaleMax,
        ballScaleSize, ballScaleMin, ballScaleMax,
        strikeScaleSize, strikeScaleMin, strikeScaleMax,
        outHitScaleMin, outScaleSize, nonHitScaleSize,
        totalStat, hitTotalPercentage;

    private string 
        currentPlayer, currentInningText, currentInning,
        firstBaseOccupant, secondBaseOccupant, thirdBaseOccupant;

    //Public Variables - TURN THESE INTO GET/SET STATEMENTS
    public float
        finalSinglePercentage, finalDoublePercentage, finalTriplePercentage, finalHomerunPercentage,
        finalBallPercentage, finalStrikePercentage, finalOutPercentage;

    //Adjustable Game Variables
    [SerializeField] float ballPercent = 0.3f, strikePercent = 0.3f;
    [SerializeField] int pitcherLevel = 10, levelModifier = 5;
    [SerializeField] int baseHitPercentage = 250;
    [SerializeField] int hitScaleMax = 1000, hitScaleMin = 1;

    //Simulation Variables
    [SerializeField] int gamesToSimulate;
    private int currentGamesPlayed;
    public int homeTeamWins, awayTeamWins;

    // Update is called once per frame
    void Update()
    {
        //Options to trigger the at-bat (Space bar/simulate)

        //Simulate multiple games at a time
        if(currentGamesPlayed < gamesToSimulate)
        {
            RunFullHitLoop();
        }

        //Press space bar to swing
        if (Input.GetKeyDown("space"))
        {
            RunFullHitLoop();
        }

    }
    public void ShowHitScale()
    {
        //Pull algorithm variables for the next batter
        RefreshVariables();
        CheckForTrick();

        //Prepare info for next batter/hit slider
        CalculatePlayerHitStats();
        CalculateMatchup();
        CalculateHitPercentages();
        EstablishHitScale();
    }
    public void RunFullHitLoop()
    {
        //Pull current algorithm variables from box score
        RefreshVariables();

        //Pull current trick modifiers if the current hitChance is a trick, set variables to 0 if not
        CheckForTrick();


        //Run HitChance algorithm and generate percentages
        CalculatePlayerHitStats();
        CalculateMatchup();
        CalculateHitPercentages();

        //Setup a 1-1000 scale using calculated percentages
        EstablishHitScale();

        //Roll 1-1000 random number and check against the hit scale, then return the final hit result
        int hitResult = RollRandomValue();
        HitResult(hitResult);

        //Display debug info
        Debug.Log("1st: " + firstBaseOccupant + " 2nd: " + secondBaseOccupant + " 3rd: " + thirdBaseOccupant);
    }

    private void CheckForTrick()
    {
        if (isTrick == true)
        {
            ResetTrickModifiers();
            RefreshTrickModifiers();
        }
        else
        {
            ResetTrickModifiers();
        }
    }

    private void RefreshVariables()
    {
        currentPlayer = boxScore.currentHitter;
        playerLevel = playerCharacterDatabase.GetPlayerLevel(currentPlayer);
        playerBaseStat = playerCharacterDatabase.GetPlayerBaseStat(currentPlayer);
        playerContactModifier = playerCharacterDatabase.GetPlayerContactModifier(currentPlayer);
        playerPowerModifier = playerCharacterDatabase.GetPlayerPowerModifier(currentPlayer);
        playerSpeedModifier = playerCharacterDatabase.GetPlayerSpeedModifier(currentPlayer);

        hitChanceSliderUI.EstablishHitScale();
    }
    private void RefreshTrickModifiers()
    {
        string currentTrickString = "Big At-Bat";
        currentTrick = tricksDatabase.GetTrick(currentTrickString);

        trickLevelModifier = currentTrick.modifiers["levelModifier"];
        trickPowerModifier = currentTrick.modifiers["powerModifier"];
        trickContactModifier = currentTrick.modifiers["contactModifier"];
        trickSpeedModifier = currentTrick.modifiers["speedModifier"];
        trickHitChanceModifier = currentTrick.modifiers["hitChanceModifier"];

        Debug.Log(trickLevelModifier);
        Debug.Log(trickPowerModifier);
        Debug.Log(trickContactModifier);
        Debug.Log(trickHitChanceModifier);
    }
    private void ResetTrickModifiers()
    {
        trickLevelModifier = 0;
        trickPowerModifier = 0;
        trickContactModifier = 0;
        trickSpeedModifier = 0;
        trickHitChanceModifier = 0;
    }
    private void HitResult(int hitResult)
    {
        //Hit results tree
        if (hitResult >= hitScaleMin && hitResult <= singleHitScaleMax)
        {
            Debug.Log("Single");
            resultTextUI.ShowResultText("Single");
            boxScore.HitSingle();
        }
        else if (hitResult >= doubleHitScaleMin && hitResult <= doubleHitScaleMax)
        {
            Debug.Log("Double");
            resultTextUI.ShowResultText("Double");
            boxScore.HitDouble();
        }
        else if (hitResult >= tripleHitScaleMin && hitResult <= tripleHitScaleMax)
        {
            Debug.Log("Triple");
            resultTextUI.ShowResultText("Triple");
            boxScore.HitTriple();
        }
        else if (hitResult >= homerunHitScaleMin && hitResult <= homerunHitScaleMax)
        {
            Debug.Log("Homerun");
            resultTextUI.ShowResultText("Homerun");
            boxScore.HitHomerun();
        }
        else if (hitResult >= ballScaleMin && hitResult <= ballScaleMax)
        {
            Debug.Log("Ball");
            resultTextUI.ShowResultText("Ball");
            boxScore.Ball();
        }
        else if (hitResult >= strikeScaleMin && hitResult <= strikeScaleMax)
        {
            Debug.Log("Strike");
            resultTextUI.ShowResultText("Strike");
            boxScore.Strike();
        }
        else if (hitResult >= outHitScaleMin && hitResult <= hitScaleMax)
        {
            Debug.Log("Out");
            resultTextUI.ShowResultText("Out");
            boxScore.HitOut();
        }
        else
        {
            Debug.Log("Error. Hit result out of bounds.");
        }
    }
    private int RollRandomValue()
    {
        //Roll random value
        return UnityEngine.Random.Range(hitScaleMin, hitScaleMax);
    }
    public void CalculatePlayerHitStats()
    {
        //Calculate Player hit Stats
        hitHomerun = playerBaseStat + playerPowerModifier + trickPowerModifier;
        hitSingle = playerBaseStat + ((playerContactModifier + trickContactModifier) * 4);
        hitDouble = playerBaseStat + ((playerContactModifier + trickContactModifier) / 2) + ((playerPowerModifier + trickPowerModifier) / 2);
        hitTriple = playerBaseStat + ((playerContactModifier + trickContactModifier) / 2) + ((playerSpeedModifier + trickSpeedModifier) / 3);

        //Add all stats
        totalStat = hitHomerun + hitDouble + hitSingle + hitTriple;
    }
    private void CalculateMatchup()
    {
        //Calculate matchup / hit percentage
        levelDifferential = ((playerLevel + trickLevelModifier) - pitcherLevel) * levelModifier;
        hitPercentage = baseHitPercentage + levelDifferential + (playerContactModifier / 2);
    }
    private void CalculateHitPercentages()
    {
        rawHomerunPercent = hitHomerun / totalStat;
        rawSinglePercent = hitSingle / totalStat;
        rawDoublePercent = hitDouble / totalStat;
        rawTriplePercent = hitTriple / totalStat;

        homerunScaleSize = Mathf.Floor(hitPercentage * rawHomerunPercent);
        singleScaleSize = Mathf.Floor(hitPercentage * rawSinglePercent);
        doubleScaleSize = Mathf.Floor(hitPercentage * rawDoublePercent);
        tripleScaleSize = Mathf.Floor(hitPercentage * rawTriplePercent);

        hitTotalPercentage = homerunScaleSize + singleScaleSize + doubleScaleSize + tripleScaleSize;

        nonHitScaleSize = 1000 - (homerunScaleSize + singleScaleSize + doubleScaleSize + tripleScaleSize);
        strikeScaleSize = Mathf.Floor(nonHitScaleSize * strikePercent);
        ballScaleSize = Mathf.Floor(nonHitScaleSize * ballPercent);
        outScaleSize = 1000 - (ballScaleSize + strikeScaleSize + homerunScaleSize + singleScaleSize + doubleScaleSize + tripleScaleSize);

        //Calculate overall percentages for slider usage

        finalSinglePercentage = singleScaleSize / 1000;
        finalDoublePercentage = doubleScaleSize / 1000;
        finalTriplePercentage = tripleScaleSize / 1000;
        finalHomerunPercentage = homerunScaleSize / 1000;
        finalBallPercentage = ballScaleSize / 1000;
        finalStrikePercentage = strikeScaleSize / 1000;
        finalOutPercentage = outScaleSize / 1000;

        //Debug.Log(finalSinglePercentage + " " + finalDoublePercentage + " " + finalTriplePercentage + " " + finalHomerunPercentage + " " + finalBallPercentage + " " + finalStrikePercentage + " " + finalOutPercentage);

        hitChanceSliderUI.EstablishHitScale();

    }
    private void EstablishHitScale()
    {
        singleHitScaleMax = singleScaleSize;

        doubleHitScaleMin = singleHitScaleMax + 1;
        doubleHitScaleMax = singleHitScaleMax + doubleScaleSize;

        tripleHitScaleMin = doubleHitScaleMax + 1;
        tripleHitScaleMax = doubleHitScaleMax + tripleScaleSize;

        homerunHitScaleMin = tripleHitScaleMax + 1;
        homerunHitScaleMax = tripleHitScaleMax + homerunScaleSize;

        ballScaleMin = homerunHitScaleMax + 1;
        ballScaleMax = homerunHitScaleMax + ballScaleSize;

        strikeScaleMin = ballScaleMax + 1;
        strikeScaleMax = ballScaleMax + strikeScaleSize;

        outHitScaleMin = strikeScaleMax + 1;
    }

    //Public Functions
    public void IsTrickTrue()
    {
        isTrick = true;
    }
    public void IsTrickFalse()
    {
        isTrick = false;
    }
}
