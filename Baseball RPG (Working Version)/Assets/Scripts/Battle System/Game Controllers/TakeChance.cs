﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeChance : MonoBehaviour
{
    [SerializeField] BoxScore boxScore;
    [SerializeField] PlayerCharacterDatabase playerCharacterDatabase;
    [SerializeField] HitChanceSliderUI hitChanceSliderUI;
    [SerializeField] ResultTextUI resultTextUI;

    //Game Configuration variables
    [SerializeField] int levelModifier;

    //Pitcher Variables - TEMPORARY, WILL LINK TO PLAYERCHARACTER
    [SerializeField] int pitcherLevel;

    //Scale Variables
    private int takeScaleMin = 1, takeScaleMax = 1000;

    private string currentPlayer;
    private int playerLevel, playerBaseStat, playerContactModifier, levelDifferential;
    private float ballChanceTotal, baseBallStat = 700, ballChancePercentage, strikeScalePercentage, strikeScaleMin, strikeScaleMax, strikeScaleSize, ballScaleMin, ballScaleMax, ballScaleSize;

    //Public final percentage for use in other scripts
    public float finalBallPercentage, finalStrikePercentage;

    //Public Functions
    public void ShowTakeScale()
    {
        //Pull algorithm variables for the current batter
        RefreshVariables();

        //Prepare info for current batter
        CalculateMatchup();
        CalculateTakePercentages();
        EstablishTakeScale();
    }
    public void RunFullTakeLoop()
    {
        //Pull current algorithm variables from box score
        RefreshVariables();

        //Run TakeChance algorithm and generate percentages
        CalculateMatchup();
        CalculateTakePercentages();

        //Setup a 1-1000 scale using calculated percentages
        EstablishTakeScale();

        //Roll 1-1000 random number and check against the Take Result tree, then return the final take result
        int takeResult = RollRandomValue();
        TakeResult(takeResult);

        //Reset Scale
        hitChanceSliderUI.ResetScale();
    }

    //Take Chance algorithm
    private void RefreshVariables()
    {
        currentPlayer = boxScore.currentHitter;
        playerLevel = playerCharacterDatabase.GetPlayerLevel(currentPlayer);
        playerBaseStat = playerCharacterDatabase.GetPlayerBaseStat(currentPlayer);
        playerContactModifier = playerCharacterDatabase.GetPlayerContactModifier(currentPlayer);

        hitChanceSliderUI.EstablishTakeScale();
    }
    private void CalculateMatchup()
    {
        //Calculate matchup / hit percentage
        levelDifferential = (playerLevel - pitcherLevel) * levelModifier;
        ballChanceTotal = baseBallStat + levelDifferential + playerContactModifier;
    }
    private void CalculateTakePercentages()
    {
        ballChancePercentage = ballChanceTotal / takeScaleMax;
        strikeScalePercentage = takeScaleMax - ballChanceTotal;

        ballScaleSize = Mathf.Floor(ballChancePercentage * takeScaleMax);
        strikeScaleSize = Mathf.Floor(strikeScalePercentage);

        finalBallPercentage = ballScaleSize / takeScaleMax;
        finalStrikePercentage = strikeScaleSize / takeScaleMax;

        hitChanceSliderUI.EstablishTakeScale();
    }
    private void EstablishTakeScale()
    {
        ballScaleMin = takeScaleMin;
        ballScaleMax = ballScaleSize;

        strikeScaleMin = ballScaleMax + 1;
        strikeScaleMax = takeScaleMax;
    }
    private int RollRandomValue()
    {
        //Roll random value
        return UnityEngine.Random.Range(takeScaleMin, takeScaleMax);
    }
    private void TakeResult(int takeResult)
    {
        //Hit results tree
        if (takeResult >= ballScaleMin && takeResult <= ballScaleMax)
        {
            Debug.Log("Ball");
            resultTextUI.ShowResultText("Ball");
            boxScore.Ball();
        }
        else if (takeResult >= strikeScaleMin && takeResult <= strikeScaleMax)
        {
            Debug.Log("Strike");
            resultTextUI.ShowResultText("Strike");
            boxScore.Strike();
        }
        else
        {
            Debug.Log("Error. Hit result out of bounds.");
        }
    }
}
