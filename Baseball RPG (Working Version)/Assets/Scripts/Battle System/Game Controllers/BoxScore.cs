﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScore : MonoBehaviour
{
    private string homeTeamName;
    private string awayTeamName;

    public int homeScore;
    public int awayScore;

    public float currentInningCounter; // One number, with decimals, that tracks inning number and top/bottom
    public float currentInning; // Holds the current inning number
    public string currentHalfInning; // Holds whether or not it is the top or bottom of an inning

    private float halfInning;

    public int outs;
    public int balls;
    public int strikes;

    public string currentHitter;
    public string currentTeam;

    public List<string> homeBattingOrder = new List<string>();
    public List<string> awayBattingOrder = new List<string>();


    public int homeCurrentHitterIndex;
    public int awayCurrentHitterIndex;

    public bool homeTeamUp;
    public bool awayTeamUp;

    [SerializeField] HitChance hitChance;
    [SerializeField] PlayerCharacterDatabase playerCharacterDatabase;


    // Start is called before the first frame update
    void Start()
    {
        SetInitialValues();
    }

    //Start of game functions

    private void SetInitialValues()
    {
        LoadBattingOrder();

        outs = 0;
        balls = 0;
        strikes = 0;
        currentInningCounter = 1;
        halfInning = 0.5f;
        homeScore = 0;
        awayScore = 0;

        awayCurrentHitterIndex = 0;
        homeCurrentHitterIndex = -1;

        homeTeamName = "Minneapolis Millers";
        awayTeamName = "New York Evils";

        homeTeamUp = false;
        awayTeamUp = true;

        UpdateCurrentInningInfo();

        currentTeam = awayTeamName;
        currentHitter = awayBattingOrder[0];
    }
    public void LoadBattingOrder()
    {
        for (int i = 1; i < 10; i++)
        {
            homeBattingOrder.Add(playerCharacterDatabase.GetBattingOrder(i, "Minneapolis Millers"));
        }
        for (int i = 1; i < 10; i++)
        {
            awayBattingOrder.Add(playerCharacterDatabase.GetBattingOrder(i, "New York Evils"));
        }
    }

    //Hit Results

    public void HitSingle()
    {
        if(playerCharacterDatabase.GetAllBaserunners() > 0)
        {
            playerCharacterDatabase.MoveBaserunnerOne(3);
            playerCharacterDatabase.MoveBaserunnerOne(2);
            playerCharacterDatabase.MoveBaserunnerOne(1);

        }

        playerCharacterDatabase.MoveHitterSingle(currentHitter);
        playerCharacterDatabase.AddCareerSingle(currentHitter);

        CalculateScore();
        NextBatter();
        SetCurrentHitter();
    }
    public void HitDouble()
    {
        if (playerCharacterDatabase.GetAllBaserunners() > 0)
        {
            playerCharacterDatabase.MoveBaserunnerTwo(1);
            playerCharacterDatabase.MoveBaserunnerHome(2);
            playerCharacterDatabase.MoveBaserunnerHome(3);
        }
        playerCharacterDatabase.MoveHitterDouble(currentHitter);
        playerCharacterDatabase.AddCareerDouble(currentHitter);

        CalculateScore();
        NextBatter();
        SetCurrentHitter();
    }
    public void HitTriple()
    {
        AllRunnersScore();

        playerCharacterDatabase.MoveHitterTriple(currentHitter);
        playerCharacterDatabase.AddCareerTriple(currentHitter);

        CalculateScore();

        NextBatter();
        SetCurrentHitter();
    }
    public void HitHomerun()
    {
        AllRunnersScore();

        playerCharacterDatabase.MoveHitterHomerun(currentHitter);
        playerCharacterDatabase.AddCareerHomerun(currentHitter);

        CalculateScore();

        NextBatter();
        SetCurrentHitter();
    }
    private void AllRunnersScore()
    {
        if (playerCharacterDatabase.GetAllBaserunners() > 0)
        {
            playerCharacterDatabase.MoveBaserunnerHome(1);
            playerCharacterDatabase.MoveBaserunnerHome(2);
            playerCharacterDatabase.MoveBaserunnerHome(3);
        }
    }

    //Balls and Strikes Results

    public void Ball()
    {
        balls++;

        if (balls == 4)
        {
            if(playerCharacterDatabase.GetBaserunnersBool(1) > 0)
            {
                if (playerCharacterDatabase.GetBaserunnersBool(2) > 0)
                {
                    if (playerCharacterDatabase.GetBaserunnersBool(3) > 0)
                    {
                        playerCharacterDatabase.MoveBaserunnerOne(3);
                    }
                    playerCharacterDatabase.MoveBaserunnerOne(2);
                }
                playerCharacterDatabase.MoveBaserunnerOne(1);
            }

            CalculateScore();

            playerCharacterDatabase.MoveHitterSingle(currentHitter);
            NextBatter();
            SetCurrentHitter();
        }
    }
    public void Strike()
    {
        strikes++;

        if (strikes == 3)
        {
            outs++;
            ThreeOuts();

            NextBatter();
            SetCurrentHitter();
        }
    }

    //Out Results

    public void HitOut()
    {
        outs++;

        ThreeOuts();

        NextBatter();
        SetCurrentHitter();
    }
    private void ThreeOuts()
    {
        if (outs >= 3)
        {
            currentInningCounter += halfInning;
            balls = 0;
            strikes = 0;
            outs = 0;

            playerCharacterDatabase.ResetCurrentBaseInningChange();

            homeTeamUp = !homeTeamUp;
            awayTeamUp = !awayTeamUp;

            UpdateCurrentInningInfo();

            AwayWinCheck();
        }
    }
    private void UpdateCurrentInningInfo()
    {
        currentInning = Mathf.Floor(currentInningCounter);

        if (homeTeamUp == true)
        {
            currentHalfInning = "Bottom";
        }
        if (awayTeamUp == true)
        {
            currentHalfInning = "Top";
        }
    }

    //Current hitter functions

    private void SetCurrentHitter()
    {
        if (awayTeamUp == true)
        {
            currentHitter = awayBattingOrder[awayCurrentHitterIndex];
            currentTeam = awayTeamName;
        }

        if (homeTeamUp == true)
        {
            currentHitter = homeBattingOrder[homeCurrentHitterIndex];
            currentTeam = homeTeamName;
        }
    }
    private void NextBatter()
    {
        balls = 0;
        strikes = 0;

        if (awayTeamUp == true)
        {
            if(awayCurrentHitterIndex < 8)
            {
                awayCurrentHitterIndex++;
            }
            else if(awayCurrentHitterIndex == 8)
            {
                awayCurrentHitterIndex = 0;
            }
        }

        if (homeTeamUp == true)
        {
            if (homeCurrentHitterIndex < 8)
            {
                homeCurrentHitterIndex++;
            }
            else if (homeCurrentHitterIndex == 8)
            {
                homeCurrentHitterIndex = 0;
            }
        }

        HomeWinCheck();
    }

    //Scoring functions

    private void CalculateScore()
    {
        if (playerCharacterDatabase.GetRunScorers() > 0)
        {
            AddScore();
            playerCharacterDatabase.ResetCurrentBaseRuns();
        }
    }
    private void AddScore()
    {
        if (awayTeamUp == true)
        {
            awayScore += playerCharacterDatabase.GetRunScorers();
        }

        if (homeTeamUp == true)
        {
            homeScore += playerCharacterDatabase.GetRunScorers();
        }
    }

    //Win check functions

    public void HomeWinCheck() //Checked every batter (in NextBatter()) during/anytime after the bottom of the ninth
    {
        if(currentInningCounter >= 9.5 && homeTeamUp == true && homeScore > awayScore)
        {
            Debug.Log("The " + homeTeamName + " win!");
            hitChance.homeTeamWins++;
            SetInitialValues();
        }
    }
    public void AwayWinCheck() //Checked only on inning changes after the ninth (in ThreeOuts())
    {
        if(currentInningCounter >= 10 && awayTeamUp == true && awayScore > homeScore)
        {
            Debug.Log("The " + awayTeamName + " win.");
            hitChance.awayTeamWins++;
            SetInitialValues();
        }
    }

    //Data return functions

    public string GetCurrentHitter()
    {
        return currentHitter;
    }
    public string GetCurrentTeam()
    {
        return currentTeam;
    }

}
