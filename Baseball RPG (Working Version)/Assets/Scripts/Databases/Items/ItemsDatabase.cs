﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsDatabase : MonoBehaviour
{
    public List<Items> items = new List<Items>();

    // Start is called before the first frame update
    private void Awake()
    {
        BuildDatabase();
        Debug.Log("Items Database Built");
    }

    public Items GetItem(string itemToGet)
    {
        return items.Find(items => items.itemName == itemToGet);
    }

    private void BuildDatabase()
    {
        items = new List<Items>()
        {
            new Items("Bat Tar",
            new Dictionary<string, string>
            {

            },
            new Dictionary<string, int>
            {
                {"levelModifier", 0},
                {"contactModifier", 100},
                {"powerModifier", 0},
                {"hitChanceModifier", 0},

            }
            ),
            new Items("Chew",
            new Dictionary<string, string>
            {

            },
            new Dictionary<string, int>
            {
                {"levelModifier", 0},
                {"contactModifier", 0},
                {"powerModifier", 0},
                {"hitChanceModifier", 100},
            }
            ),
            new Items("Chewing Gum",
            new Dictionary<string, string>
            {

            },
            new Dictionary<string, int>
            {
                {"levelModifier", 5},
                {"contactModifier", 0},
                {"powerModifier", 0},
                {"hitChanceModifier", 0},
            }
            ),
        };
    }
}
