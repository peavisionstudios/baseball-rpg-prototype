﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items
{
    public string itemName;
    public Dictionary<string, string> itemInformation = new Dictionary<string, string>();
    public Dictionary<string, int> modifiers = new Dictionary<string, int>();


    public Items(string itemName, Dictionary<string, string> itemInformation, Dictionary<string, int> modifiers)
    {
        this.itemName = itemName;
        this.itemInformation = itemInformation;
        this.modifiers = modifiers;
    }
    public Items(Items items)
    {
        this.itemName = items.itemName;
        this.itemInformation = items.itemInformation;
        this.modifiers = items.modifiers;
    }
}

