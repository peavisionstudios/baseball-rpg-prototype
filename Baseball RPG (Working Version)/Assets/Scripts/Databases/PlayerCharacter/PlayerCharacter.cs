﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter
{
    public string characterName;
    public string currentTeam;
    public int battingOrder;
    public int currentBase;

    public Dictionary<string, int> stats = new Dictionary<string, int>();
    public Dictionary<string, string> playerInformation = new Dictionary<string, string>();

    public PlayerCharacter(string characterName, string currentTeam, int battingOrder, int currentBase, Dictionary<string, int> stats, Dictionary<string, string> playerInformation)
    {
        this.characterName = characterName;
        this.currentTeam = currentTeam;
        this.battingOrder = battingOrder;
        this.currentBase = currentBase;
        this.stats = stats;
        this.playerInformation = playerInformation;
    }
    public PlayerCharacter(PlayerCharacter playerCharacter)
    {
        this.characterName = playerCharacter.characterName;
        this.currentTeam = playerCharacter.currentTeam;
        this.battingOrder = playerCharacter.battingOrder;
        this.currentBase = playerCharacter.currentBase;
        this.stats = playerCharacter.stats;
        this.playerInformation = playerCharacter.playerInformation;
    }

}
