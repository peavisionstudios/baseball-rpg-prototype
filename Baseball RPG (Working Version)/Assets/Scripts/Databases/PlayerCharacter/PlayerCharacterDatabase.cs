﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterDatabase : MonoBehaviour
{
    public List<PlayerCharacter> playerCharacters = new List<PlayerCharacter>();

    //Build database on Awake()
    private void Awake()
    {
        BuildDatabase();
        Debug.Log("PlayerCharacter Database Built");
    }

    //Indexing Functions
    //Player Character Functions

    public PlayerCharacter GetPlayerCharacter(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
    }

    //Baserunning functions

    public PlayerCharacter GetBaserunners(int currentBase)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase);
    }
    public string GetBaserunnersName(int currentBase)
    {

        if(playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase) != null)
        {
            return playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase).characterName;
        }
        else
        {
            return "Empty";
        }
    }
    public int GetBaserunnersBool(int currentBase)
    {
        return playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase == currentBase).Count;
    }
    public void MoveBaserunnerOne(int currentBase)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase++;
            Debug.Log(tempPlayerCharacter.characterName + " Base: " + tempPlayerCharacter.currentBase);
        }
    }
    public void MoveBaserunnerTwo(int currentBase)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase += 2;
        }
    }
    public void MoveBaserunnerThree(int currentBase)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase += 3;
        }
    }
    public void MoveBaserunnerHome(int currentBase)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.currentBase == currentBase);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase = 4;
        }
    }

    //Hitter functions

    public void MoveHitterSingle(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase = 1;
        }
    }
    public void MoveHitterDouble(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase = 2;
        }
    }
    public void MoveHitterTriple(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase = 3;
        }
    }
    public void MoveHitterHomerun(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.currentBase = 4;
        }
    }

    //Historic hitting data

    public void AddCareerSingle(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.stats["Singles"]++;
        }
    }
    public void AddCareerDouble(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.stats["Doubles"]++;
        }
    }
    public void AddCareerTriple(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.stats["Triples"]++;
        }
    }
    public void AddCareerHomerun(string characterName)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName);
        if (tempPlayerCharacter != null)
        {
            tempPlayerCharacter.stats["Homeruns"]++;
        }
    }

    //Data Retrieval Functions

    public int GetPlayerLevel(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).stats["Level"];
    }
    public int GetPlayerBaseStat(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).stats["Base Stat"];
    }
    public int GetPlayerContactModifier(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).stats["Contact"];

    }
    public int GetPlayerPowerModifier(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).stats["Power"];

    }
    public int GetPlayerSpeedModifier(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).stats["Speed"];

    }
    public int GetCareerStats(string characterName, string statToFind)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).stats[statToFind];
    }
    public string GetPlayerTeam(string characterName)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.characterName == characterName).currentTeam;
    }
    public string GetBattingOrder(int battingOrder, string currentTeam)
    {
        return playerCharacters.Find(playerCharacter => playerCharacter.battingOrder == battingOrder && playerCharacter.currentTeam == currentTeam).characterName;
    }
    public int GetRunScorers()
    {
        return playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase >= 4).Count;
    }
    public int GetAllBaserunners()
    {
        return playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase > 0).Count;
    }
    public bool GetBaseOccupiedBool(int baseToSearch)
    {
        PlayerCharacter tempPlayerCharacter = playerCharacters.Find(playerCharacter => playerCharacter.currentBase == baseToSearch);
        if (tempPlayerCharacter != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Reset current base functions

    public void ResetCurrentBaseRuns()
    {
        int totalScorers = playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase >= 4).Count;
        List<PlayerCharacter> runScorerList = playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase >= 4);
        for (int i = 0; i < totalScorers; i++)
        {
            runScorerList[i].currentBase = 0;
        }

    }
    public void ResetCurrentBaseInningChange()
    {
        int totalBaserunners = playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase > 0).Count;
        List<PlayerCharacter> baseRunnerList = playerCharacters.FindAll(playerCharacter => playerCharacter.currentBase > 0);
        for (int i = 0; i < totalBaserunners; i++)
        {
            baseRunnerList[i].currentBase = 0;
        }

    }

    //PlayerCharacter Database
    void BuildDatabase()
    {
        playerCharacters = new List<PlayerCharacter>()
        {
            //Player Team - Minneapolis Millers
            new PlayerCharacter("Paul", "Minneapolis Millers", 1, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 25},
                {"Base Stat", 10},
                {"Contact", 50},
                {"Power", 30},
                {"Speed", 15},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Paul Robert Veit"},
                {"Uniform Number", "16"},
                {"Age", "31"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "CF"},
                {"Handedness", "Right" },
                {"Background",
                    "The unlikely hero of this bizarre baseball journey. Has a real knack for jazzing people up." },
            }
            ),
            new PlayerCharacter("Pepper", "Minneapolis Millers", 5, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 11},
                {"Base Stat", 10},
                {"Contact", 40},
                {"Power", 5},
                {"Speed", 20},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Pepper"},
                {"Uniform Number", "2"},
                {"Age", "1176"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "DH"},
                {"Handedness", "Right" },
                {"Background",
                    "An ancient baseball playing dragon that has bestowed their powers on the Hero's team. Identifies as a dr0nekid." },
            }
            ),
            new PlayerCharacter("Jenny", "Minneapolis Millers", 4, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 12},
                {"Base Stat", 10},
                {"Contact", 5},
                {"Power", 40},
                {"Speed", 5},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Jenny Marie Veit"},
                {"Uniform Number", "21"},
                {"Age", "30"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "3B"},
                {"Handedness", "Right" },
                {"Background",
                    "Tagged along for the ride, but has been enjoying herself so far." },
            }
            ),
            new PlayerCharacter("Jane", "Minneapolis Millers", 6, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 10},
                {"Base Stat", 10},
                {"Contact", 20},
                {"Power", 30},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Jane Do Os"},
                {"Uniform Number", "42"},
                {"Age", "31"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "C"},
                {"Handedness", "Right" },
                {"Background",
                    "Never shy from letting the ump know when they got it wrong. Problem is she's always right." },
            }
            ),
            new PlayerCharacter("Pat", "Minneapolis Millers", 8, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 11},
                {"Base Stat", 10},
                {"Contact", 5},
                {"Power", 40},
                {"Speed", 5},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Pat Pat Moore"},
                {"Uniform Number", "2"},
                {"Age", "32"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "1B"},
                {"Handedness", "Right" },
                {"Background",
                    "Huge hands. He does NOT go by Pat Pat." },
            }
            ),
            new PlayerCharacter("Kyle", "Minneapolis Millers", 9, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 9},
                {"Base Stat", 10},
                {"Contact", 30},
                {"Power", 15},
                {"Speed", 20},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Kyle Roosevelt Jackson"},
                {"Uniform Number", "17"},
                {"Age", "26"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "LF"},
                {"Handedness", "Left" },
                {"Background",
                    "Fast as lightning, but a gentler soul there is not. Makes the rest of the group smile with his youthful antics." },
            }
            ),
            new PlayerCharacter("Renee", "Minneapolis Millers", 7, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 10},
                {"Base Stat", 10},
                {"Contact", 30},
                {"Power", 20},
                {"Speed", 20},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Renee Delanor Jackson"},
                {"Uniform Number", "9"},
                {"Age", "26"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "RF"},
                {"Handedness", "Left" },
                {"Background",
                    "Extremely calculated with eyes like an eagle. Never an easy out." },
            }
            ),
            new PlayerCharacter("Ben", "Minneapolis Millers", 2, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 11},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 30},
                {"Speed", 30},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Ben Richman Peters"},
                {"Uniform Number", "6"},
                {"Age", "31"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "2B"},
                {"Handedness", "Right" },
                {"Background",
                    "Gentle giant. Only uses his bat smashing powers against baseballs and fascists." },
            }
            ),
            new PlayerCharacter("Dominique", "Minneapolis Millers", 3, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 8},
                {"Base Stat", 10},
                {"Contact", 40},
                {"Power", 5},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Dominique"},
                {"Uniform Number", "99"},
                {"Age", "27"},
                {"Current Team", "Minneapolis Millers"},
                {"Current Position", "SS"},
                {"Handedness", "Right" },
                {"Background",
                    "Don't let her short stature mistake you, her hands are bullet quick and it shows both in the field and at bat." },
            }
            ),

            //Enemy Team #1 - New York Evils
            new PlayerCharacter("Hotrod", "New York Evils", 1, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 10},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Hotrod"},
                {"Uniform Number", "69"},
                {"Age", "23"},
                {"Current Team", "New York Evils"},
                {"Current Position", "SS"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Blaze", "New York Evils", 5, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 11},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Blaze"},
                {"Uniform Number", "69"},
                {"Age", "22"},
                {"Current Team", "New York Evils"},
                {"Current Position", "2B"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Scythe", "New York Evils", 4, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 12},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Scythe"},
                {"Uniform Number", "69"},
                {"Age", "22"},
                {"Current Team", "New York Evils"},
                {"Current Position", "1B"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Rommer", "New York Evils", 6, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 10},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Rommer"},
                {"Uniform Number", "69"},
                {"Age", "21"},
                {"Current Team", "New York Evils"},
                {"Current Position", "3B"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Flax", "New York Evils", 8, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 8},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Flax"},
                {"Uniform Number", "69"},
                {"Age", "22"},
                {"Current Team", "New York Evils"},
                {"Current Position", "C"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Borno", "New York Evils", 9, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 7},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Borno"},
                {"Uniform Number", "69"},
                {"Age", "24"},
                {"Current Team", "New York Evils"},
                {"Current Position", "LF"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Crispy", "New York Evils", 7, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 9},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Crispy"},
                {"Uniform Number", "69"},
                {"Age", "22"},
                {"Current Team", "New York Evils"},
                {"Current Position", "CF"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Bonez", "New York Evils", 2, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 10},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Bonez"},
                {"Uniform Number", "RF"},
                {"Age", "22"},
                {"Current Team", "New York Evils"},
                {"Current Position", "2B"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),
            new PlayerCharacter("Bullhawk", "New York Evils", 3, 0,
            new Dictionary<string, int>
            {
                //Player Character RPG level/stats
                {"Level", 9},
                {"Base Stat", 10},
                {"Contact", 10},
                {"Power", 10},
                {"Speed", 10},
                {"Team Cheer", 10},
                {"Fielding", 10},

                //Player Character career stats
                {"Singles", 0},
                {"Doubles", 0},
                {"Triples", 0},
                {"Homeruns", 0}
            },
            new Dictionary<string, string>
            {
                //Player Information
                {"Full Name", "Bullhawk"},
                {"Uniform Number", "69"},
                {"Age", "22"},
                {"Current Team", "New York Evils"},
                {"Current Position", "DH"},
                {"Handedness", "Right" },
                {"Background",
                    "Generic NYC Bro." },
            }
            ),

        };
    }
}
