﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TricksDatabase : MonoBehaviour
{
    public List<Tricks> tricks = new List<Tricks>();

    // Start is called before the first frame update
    private void Awake()
    {
        BuildDatabase();
        Debug.Log("Tricks Database Built");
    }

    public Tricks GetTrick(string trickToGet)
    {
        return tricks.Find(tricks => tricks.trickName == trickToGet);
    }

    private void BuildDatabase()
    {
        tricks = new List<Tricks>()
        {
            //Hitting Abilities
            new Tricks("Lightning Hands",
            new Dictionary<string, string>
            {

            },
            new Dictionary<string, int>
            {
                {"levelModifier", 0},
                {"contactModifier", 100},
                {"powerModifier", 0},
                {"speedModifier", 0},
                {"hitChanceModifier", 0},

            }
            ),
            new Tricks("Eagle Eye",
            new Dictionary<string, string>
            {

            },
            new Dictionary<string, int>
            {
                {"levelModifier", 0},
                {"contactModifier", 0},
                {"powerModifier", 0},
                {"speedModifier", 0},
                {"hitChanceModifier", 100},
            }
            ),
            new Tricks("Big At-Bat",
            new Dictionary<string, string>
            {

            },
            new Dictionary<string, int>
            {
                {"levelModifier", 20},
                {"contactModifier", 0},
                {"powerModifier", 0},
                {"speedModifier", 0},
                {"hitChanceModifier", 0},
            }
            ),
        };
    }
}
