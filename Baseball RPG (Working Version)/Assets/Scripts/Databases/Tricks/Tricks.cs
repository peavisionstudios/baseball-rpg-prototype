﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tricks
{
    public string trickName;
    public Dictionary<string, string> trickInformation = new Dictionary<string, string>();
    public Dictionary<string, int> modifiers = new Dictionary<string, int>();


    public Tricks(string trickName, Dictionary<string, string> trickInformation, Dictionary<string, int> modifiers)
    {
        this.trickName = trickName;
        this.trickInformation = trickInformation;
        this.modifiers = modifiers;
    }
    public Tricks(Tricks tricks)
    {
        this.trickName = tricks.trickName;
        this.trickInformation = tricks.trickInformation;
        this.modifiers = tricks.modifiers;
    }
}
